import React, { Component } from "react";
import ItemPhone from "./ItemPhone";

class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.list.map((item, index) => {
      return (
        <ItemPhone
          data={item}
          key={index}
          changePhone2={this.props.changePhone1}
        />
      );
    });
  };
  render() {
    return (
      <div>
        <h2>List Phone</h2>
        <div className="container">
          <div className="row">{this.renderListPhone()}</div>
        </div>
      </div>
    );
  }
}

export default ListPhone;
