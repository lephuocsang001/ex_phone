import React, { Component } from "react";
import { data_phone } from "./data_Phone";
import DetailPhone from "./DetailPhone";
import ItemPhone from "./ItemPhone";
import ListPhone from "./ListPhone";

class EX_Phone extends Component {
  state = {
    list: data_phone,
    phone: data_phone[0],
  };
  changePhone = (newPhone) => {
    this.setState({ phone: newPhone });
  };
  render() {
    return (
      <div>
        <ListPhone changePhone1={this.changePhone} list={this.state.list} />
        <DetailPhone phone={this.state.phone} />
      </div>
    );
  }
}

export default EX_Phone;
