import logo from "./logo.svg";
import "./App.css";
import EX_Phone from "./EX_Phone/EX_Phone";

function App() {
  return (
    <div className="App">
      <EX_Phone />
    </div>
  );
}

export default App;
